#version 140

uniform sampler2D u_position_tex;
uniform sampler2D u_normal_tex;
uniform sampler2D u_color_tex;
uniform sampler2D u_depth_tex;

uniform sampler2DArrayShadow u_shadow_tex;

uniform vec3 u_camera_pos;
uniform vec3 u_light_direction;
uniform vec3 u_light_color;
uniform vec3[6] u_shadow_positions;

uniform float u_z_far;

uniform bool u_has_shadows;

in vec2 v_tex_coord;
in vec3 v_world_dir;
in vec3[6] v_shadow_world_dirs;

out vec3 out_color;

const float SHADOW_BIAS = exp2(-23);
const float SHADOW_SLOPE_BIAS = exp2(-22);

const float PI = 3.14159265358979323;

bool saturated(vec3 v) {
    return v.x >= 0.0 && v.x <= 1.0 &&
           v.y >= 0.0 && v.y <= 1.0 &&
           v.z >= 0.0 && v.z <= 1.0;
}

void main() {
    
    // retrieve data from G-buffer
    vec3 texel_pos = texture(u_position_tex, v_tex_coord).rgb;
    vec3 normal = texture(u_normal_tex, v_tex_coord).rgb;
    vec3 color = texture(u_color_tex, v_tex_coord).rgb;
    float depth = texture(u_depth_tex, v_tex_coord).r * u_z_far;
    
    if (length(normal) == 0)
        discard;

    out_color = vec3(0, 0, 0);
    
    float cosang = dot(normal, u_light_direction);

    if (cosang <= 0.0)
        return;

    float shadow;
    if (u_has_shadows) {
        //Choose best shadow cascade.
        int shadow_map;
        vec3 shadow_pos;
        for (shadow_map = 0; shadow_map < 6; shadow_map++) {
            shadow_pos = (u_shadow_positions[shadow_map] + v_shadow_world_dirs[shadow_map] * depth) * 0.5 + 0.5;
            if (saturated(shadow_pos)) break;
        }
        
        //Shadow mapping.
        float slope = sqrt(1.0 - cosang * cosang) / cosang;
        shadow_pos.z -= (slope * SHADOW_SLOPE_BIAS + SHADOW_BIAS) * exp(shadow_map * 1.7);
        shadow = texture(u_shadow_tex, vec4(shadow_pos.xy, shadow_map, shadow_pos.z));
    } else shadow = 1.0;
    
    if (shadow <= 0.001) return;
    
    out_color += max(dot(normal, u_light_direction), 0.0) * shadow * color * u_light_color;
}