package net.angle.myriad;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.osgi.framework.launch.FrameworkFactory;
import java.util.ServiceLoader;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.framework.Constants;
import org.osgi.framework.launch.Framework;

/**
 *
 * @author angle
 */
public class Myriad {
    private final Path root;
    private final Path root_lib;
    private final Path root_cache;
    
    private final FrameworkFactory frameworkFactory;
    private final Map<String, Object> config;
    
    private final Framework framework;

    public Myriad(String rootPath, String libDir, String cacheDir) throws IOException, BundleException {
        root = Files.createDirectories(Paths.get(rootPath));
        root_lib = Files.createDirectories(root.resolve(libDir));
        root_cache = Files.createDirectories(root.resolve(cacheDir));
        
        frameworkFactory =
            ServiceLoader.load(FrameworkFactory.class).iterator().next();
        
        config = new HashMap<>();
        config.put(Constants.FRAMEWORK_STORAGE, root_cache.toString());
        config.put(Constants.FRAMEWORK_STORAGE_CLEAN, "onFirstInit");
        
        final Object cast = config;
        @SuppressWarnings("unchecked")
        final Map<String, String> config_strings = (Map<String, String>) cast;
        
        framework = frameworkFactory.newFramework(config_strings);
    }
    
    public void run() throws InterruptedException, BundleException {
        framework.start();
        
        try {
            final BundleContext c = framework.getBundleContext();

            /*
             * Install all of the bundles.
             */
            
            final List<Bundle> bundles = new LinkedList<>();
            for (File file: root_lib.toFile().listFiles()) {
                try {
                    String name = file.getName();
                    if (name.endsWith(".jar"))
                        bundles.add(Myriad.install(c, root_lib, name.substring(0, name.length() -4)));
                } catch (BundleException ex) {
                    Logger.getLogger(Myriad.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            for (final Bundle bundle : bundles) {
                try {
                    bundle.start();
                } catch (BundleException ex) {
                    Logger.getLogger(Myriad.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        } finally {
            framework.stop();
            framework.waitForStop(
                TimeUnit.MILLISECONDS.convert(2L, TimeUnit.SECONDS));
        }
    }

    private static Bundle install(BundleContext c, Path lib, String pack) throws BundleException {
        final String file = "file:" + lib + "/" + pack + ".jar";
        return c.installBundle(file);
    }
}